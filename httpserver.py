from flask import Flask
import threading
import time
import requests

app = Flask(__name__)

host = port = []

@app.route('/ping')
def index():
    return 'pong'
    
def get_http():    
    with open('peer', 'r') as file:
        data = file.read().replace('\n', '')
        hosts = data.split(",")
        num = len(hosts)
        host = []*num
        port = []*num
        for i in hosts:
            host_port = i.split(":")
            host.append(host_port[0])
            port.append(host_port[1])      
    
    while True:    
        host_num = len(host)
        for i in range(host_num):
            url = "http://" + str(host[i]) + "/ping:" + str(port[i])
            #print(url)
            #url = "http://w3schools.com:80"
            get = requests.get(url)
            print(get.text)
        time.sleep(2)

if __name__ == '__main__':
    ping_thread = threading.Thread(target=get_http)
    ping_thread.daemon = True
    ping_thread.start()
    app.run(debug=True, host='0.0.0.0',port = 8088)

