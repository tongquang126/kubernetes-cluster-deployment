FROM python:3.10.3-alpine

ENV MICRO_SERVICE=/home/app/webapp
# set work directory
RUN mkdir -p $MICRO_SERVICE

# copy code
ADD . $MICRO_SERVICE


# where your code lives
WORKDIR $MICRO_SERVICE

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip
RUN pip install -U Flask
RUN pip install requests

#CMD run httpserver.py
CMD [ "python", "httpserver.py"]
#CMD ["sh", "-c", "sleep 240 && python httpserver.py"]


