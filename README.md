# About the project

## The project to create a simple http server 
* It provided a GET /ping route. Invoking this route should respond "pong" back to the client
* It invokes the other hosts’ GET /ping route every Nth amount of time (e.g. every 1 minute.)
* It is able to load a configuration containing a set of given hosts and ports 

## Prerequisite
* You must have GKE cluster, and install GitLab agent to safety connect to the GitLab (Refer GitLab CICD with a Kubernetes cluster.pdf)

![image](/uploads/b524e7076b814d6a8b1c03efbaccf381/image.png)

## The CICD Pipeline 
* Pipeline flow: GitLab Repo ---[trigger]---> GitLab CI ---[Build and push docker image]---> GitLab Container Registry ---[deploy] ---> Deploy crds on GKE cluster ---[deploy] ---> Deploy http server on GKE cluster

  ![image](/uploads/ba7308248a576bf8301f68da173ad523/image.png)

* Pipeline stages
  - Build and push docker image: Build docker image from Dockerfile,then push to GitLab Container Registry. I use Python to build the http server. the interval time for send GET /ping request to other hosts = 2 seconds   
  - Deploy crd on gke: in this stage, I create the CustomResourceDefinitions to expend the K8s API. 
    + CRD's spec will define  the peer host&port for ping check, the number of http server pod you want to deploy

    ![image](/uploads/4e5be03a43d9c065bff754763d4ca2e4/image.png)

  - Deploy http server on gke: This stage to deploy the http server to meet the desired state ( number of pod, peer hosts&ports)
    + We need to fetch the CRD's spec to know the desired pod number (replcas) and a set of hosts&ports (peer) 
    + I uses kubectl to deploy the http server, the number of pod = replicas in CRD's spec. Changing the desired number of pod does not cause the current pods recreate or restart
    + A set of hosts&ports will be added into the configmap called "my-configmap". This configmap is used as Pod's volume. Changing the hosts&ports also does not cause the current pods recreate or restart

## Verification
* After running the pipeline, the application will be deployed on GKE cluster. The pod number is as same as you desire 
  ![image](/uploads/e28df464281ba1ca7142b3bc3e424ead/image.png)

* You can expose the http server via LoadBalancer, then send GET /ping from your local host

  ![image](/uploads/f3afaef8af79bd87fd3d47be8389b85a/image.png)

  ![image](/uploads/a13ce42b30b0cd1b1723f32a52530cf1/image.png)

* Run the kubetl logs to get the logs from http server pod

  ![image](/uploads/f84dc66274409e2a228779bff1d5fbc5/image.png)

* To change the number of pod, hosts&ports, you go to yaml/my-crd.yaml, and change the CRD's spec. After that, re-run the pipeline. 

## Note 
* Re-run pipeline will not restart or recreate the pods. 
* Another solution: You can build the Kubernetes Operator to meet the requirement  



